def solution1(input: str) -> int:
    return input.count("(") - input.count(")")


def solution2(input: str) -> int:
    count = 0
    for idx, char in enumerate(input):
        count += 1 if char == "(" else -1
        if count < 0:
            return idx + 1
    return -1
    