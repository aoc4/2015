import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = "(())"
        expected = 0
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_2(self):
        test_string = "()()"
        expected = 0
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_3(self):
        test_string = "((("
        expected = 3
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_4(self):
        test_string = "(()(()("
        expected = 3
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_5(self):
        test_string = "))((((("
        expected = 3
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_6(self):
        test_string = "())"
        expected = -1
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_7(self):
        test_string = "))("
        expected = -1
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_8(self):
        test_string = ")))"
        expected = -3
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_9(self):
        test_string = ")())())"
        expected = -3
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution2_1(self):
        test_string = ")"
        expected = 1
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_2(self):
        test_string = "()())"
        expected = 5
        self.assertEqual(day.solution2(test_string), expected)