from pathlib import Path
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    for _ in range(40):
        contents = day.solution1(contents)

    print(f"solution1: {len(contents)}")

    for _ in range(10):
        contents = day.solution1(contents)

    print(f"solution2: {len(contents)}")