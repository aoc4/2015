from itertools import groupby


def solution1(input: str) -> str:
    return "".join(f"{len(list(group))}{num}" for num, group in groupby(input))