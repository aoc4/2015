import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input = "1"
        expected = "11"
        self.assertEqual(expected, day.solution1(input))

    def test11(self):
        input = "11"
        expected = "21"
        self.assertEqual(expected, day.solution1(input))

    def test21(self):
        input = "21"
        expected = "1211"
        self.assertEqual(expected, day.solution1(input))

    def test1211(self):
        input = "1211"
        expected = "111221"
        self.assertEqual(expected, day.solution1(input))

    def test111221(self):
        input = "111221"
        expected = "312211"
        self.assertEqual(expected, day.solution1(input))
