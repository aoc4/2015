from pathlib import Path
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    ans1 = day.solution1(contents)

    print(f"solution1: {ans1}")

    ans2 = day.solution1(ans1)

    print(f"solution2: {ans2}")
