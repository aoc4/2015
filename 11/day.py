from typing import List

import string


blacklist = ["i", "o", "l"]


def increase_by_blacklist(input: str) -> List[str]:
    # optimize by increase whole string if blacklist item exists
    indices = [input.find(char) for char in blacklist if char in input]

    if not indices:
        return list(input)

    min_index = min(indices)

    if min_index >= 0:
        new_letter = increase_letter(input[min_index])
        letters_after = len(input) - min_index - 1
        return list(input[:min_index] + new_letter + string.ascii_lowercase[0] * letters_after)


def increase_string(input: str) -> str:
    optimized = increase_by_blacklist(input)

    index = len(optimized) - 1
    increased = increase_letter(optimized[index])
    optimized[index] = increased
    while increased == string.ascii_lowercase[0]:
        index -= 1
        if index >= 0:
            increased = increase_letter(optimized[index])
            optimized[index] = increased
        else:
            raise ValueError("Can't increase more")
    return "".join(optimized)


def increase_letter(letter: str) -> str:
    letter_index: int = string.ascii_lowercase.find(letter)
    next_letter_index = (letter_index + 1) % len(string.ascii_lowercase)
    return string.ascii_lowercase[next_letter_index]


def is_increasing(a: str, b: str, c: str) -> bool:
    return ord(c) - ord(b) == 1 and ord(b) - ord(a) == 1


def is_valid(input: str) -> bool:
    # must include one increasing straight of at least three letters
    increasing_straight: bool = any(is_increasing(a, b, c) 
                                    for a, b, c in zip(input, input[1:], input[2:]))

    # may not contain the letters i, o, or l
    no_blacklisted: bool = not any(char in input for char in blacklist)

    # must contain at least two different, non-overlapping pairs of letters
    pairs = zip(input, input[1:])
    same_char_pairs = [(a, b) for a, b in pairs if a == b]
    # non overlapping is satisfied if pairs are different (requirement)
    two_or_more_unique_pairs = len(set(same_char_pairs)) >= 2

    return increasing_straight and no_blacklisted and two_or_more_unique_pairs


def solution1(input: str) -> str:
    new_password = increase_string(input)
    while not is_valid(new_password):
        new_password = increase_string(new_password)
    return new_password


if __name__ == "__main__":
    solution1("ghijklmn")
