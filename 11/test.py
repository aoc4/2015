import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input = "abcdefgh"
        expected = "abcdffaa"
        self.assertEqual(expected, day.solution1(input))

    def test2(self):
        input = "ghijklmn"
        expected = "ghjaabcc"
        self.assertEqual(expected, day.solution1(input))

    def test3(self):
        input = "hepxxyzz"
        expected = "heqaabcc"
        self.assertEqual(expected, day.solution1(input))
