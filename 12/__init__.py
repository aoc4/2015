from pathlib import Path
import json

from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        obj = json.loads(f.read())
        
    print(f"solution1: {day.solution1(obj)}")
    print(f"solution2: {day.solution2(obj)}")
