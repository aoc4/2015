from typing import Dict, List


def scan_dict2(obj: Dict[object, object]) -> int:
    # scan dictionary for numbers
    values = obj.values()

    if "red" in values:
        return 0
    
    return sum(scan_sum(item) for item in values)



def scan_sum(obj: object) -> int:
    if isinstance(obj, dict):
        return sum(scan_sum(item) for item in obj.values())
    elif isinstance(obj, list):
        return sum(scan_sum(item) for item in obj)
    elif isinstance(obj, int):
        return obj

    return 0

def scan_sum2(obj: object) -> int:
    if isinstance(obj, dict):
        values = obj.values()

        if "red" in values:
            return 0
        
        return sum(scan_sum2(item) for item in values)
    elif isinstance(obj, list):
        return sum(scan_sum2(item) for item in obj)
    elif isinstance(obj, int):
        return obj

    return 0
    

def solution1(obj) -> int:
    return scan_sum(obj)


def solution2(obj) -> int:
    return scan_sum2(obj)