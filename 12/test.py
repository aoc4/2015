import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input = [1, 2, 3]
        expected = 6
        self.assertEqual(expected, day.solution1(input))

    def test2(self):
        input = {"a": 2, "b": 4}
        expected = 6
        self.assertEqual(expected, day.solution1(input))

    def test3(self):
        input = [[[3]]]
        expected = 3
        self.assertEqual(expected, day.solution1(input))

    def test4(self):
        input = {"a": {"b": 4}, "c": -1}
        expected = 3
        self.assertEqual(expected, day.solution1(input))

    def test5(self):
        input = {"a": [-1, 1]}
        expected = 0
        self.assertEqual(expected, day.solution1(input))

    def test6(self):
        input = [-1, {"a": 1}]
        expected = 0
        self.assertEqual(expected, day.solution1(input))

    def test7(self):
        input = []
        expected = 0
        self.assertEqual(expected, day.solution1(input))

    def test8(self):
        input = {}
        expected = 0
        self.assertEqual(expected, day.solution1(input))
