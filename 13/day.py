from itertools import permutations
from typing import Dict, List, Set, Tuple

import re


def get_line_data(line: str) -> Tuple[str, str, int]:
    matches = re.match(r"(\S+) would (gain|lose) (\d+) happiness units by sitting next to (\S+).", line)
    source = matches.group(1)
    sign = matches.group(2)
    score = int(matches.group(3))
    score = score if sign == "gain" else -score
    target = matches.group(4)
    return source, target, score


def get_persons_and_scores(input: str) -> Tuple[Set[str], Dict[str, Dict[str, int]]]:
    persons = set()
    scores: Dict[str, Dict[str, int]] = {}
    for line in input.splitlines():
        src, trg, score = get_line_data(line)
        persons.add(src)

        if src not in scores:
            scores[src] = {}

        scores[src][trg] = score

    return persons, scores


def get_hiscore(persons: Set[str], scores: Dict[str, Dict[str, int]]) -> int:
    highest_score = None
    for permutation in permutations(persons):
        score = sum(scores[a][b] + scores[b][a] for a, b in zip(permutation, permutation[1:]))
        score += scores[permutation[-1]][permutation[0]] + scores[permutation[0]][permutation[-1]]  # round table
        highest_score = max(highest_score, score) if highest_score else score

    return highest_score


def solution1(input: str) -> int:
    persons, scores = get_persons_and_scores(input)

    return get_hiscore(persons, scores)

def solution2(input: str) -> int:
    persons, scores = get_persons_and_scores(input)

    # add me
    persons.add("Me")
    scores["Me"] = {}
    for person in persons:
        scores[person]["Me"] = 0
        scores["Me"][person] = 0

    return get_hiscore(persons, scores)