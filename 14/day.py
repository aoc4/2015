from typing import Dict, List, Tuple

import re

class Reindeer:
    def __init__(self, speed: int, duration: int, rest: int):
        self.speed = speed
        self.duration = duration
        self.rest = rest
        self.cycle = duration + rest
        self.points: int = 0

    def get_distance(self, second: int) -> int:
        leftover = second % self.cycle
        cycles = second // self.cycle
        return cycles * self.speed * self.duration + min(self.duration, leftover) * self.speed


def parse_line(line: str) -> Tuple[str, int, int, int]:
    match = re.match(r"(\S+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.", line)
    return match.group(1), int(match.group(2)), int(match.group(3)), int(match.group(4))


def solution1(input: str, seconds: int) -> int:
    max_distance = None
    for line in input.splitlines():
        _, speed, duration, rest = parse_line(line)
        r = Reindeer(speed, duration, rest)
        distance = r.get_distance(seconds)
        max_distance = distance if max_distance is None else max(distance, max_distance)
    
    return max_distance


def get_furthest_reindeer(reindeers: List[Reindeer], second: int) -> Reindeer:
    max_idx = 0 
    max_distance = reindeers[max_idx].get_distance(second)
    for idx in range(len(reindeers)):
        current_distance = reindeers[idx].get_distance(second)
        max_idx = idx if current_distance > max_distance else max_idx
        max_distance = max(max_distance, current_distance)

    return reindeers[max_idx]


def solution2(input: str, seconds: int) -> int:
    reindeers: List[Reindeer] = []
    for line in input.splitlines():
        _, speed, duration, rest = parse_line(line)
        reindeers.append(Reindeer(speed, duration, rest))

    for second in range(1, seconds):
        reindeer = get_furthest_reindeer(reindeers, second)
        reindeer.points += 1

    return max(reindeers, key = lambda r: r.points).points
