import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        second = 1000
        input = """Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."""
        expected = 1120
        self.assertEqual(expected, day.solution1(input, second))
