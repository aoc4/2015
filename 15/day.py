from typing import Iterator, List, Tuple

import re

from itertools import permutations
from math import prod


def parse_line(line: str) -> Tuple[int, int, int, int, int]:
    match = re.match("\S+: capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)", line)
    return int(match.group(1)), int(match.group(2)), int(match.group(3)), int(match.group(4)), int(match.group(5))


def get_permutations(amount: int, total: int = 100) -> Iterator[Tuple[int, ...]]:
    """Get recipe permutation.
    Sum of all elements has to be equal to 100."""
    for permutation in permutations(range(total + 1), amount):
        if sum(permutation) == total:
            yield permutation


def get_total(ingredients: List[Tuple[int, int, int, int, int]], proportions: Tuple[int, ...]) -> int:
    scaled_ingredients = [[item * proportion for item in ingredient] for ingredient, proportion in zip(ingredients, proportions)]

    return prod([max(0, sum(items)) for items in zip(*scaled_ingredients)])


def get_calories(calories: List[int], proportions: Tuple[int, ...]) -> int:
    return sum(cal * prop for cal, prop in zip(calories, proportions))


def solution1(input: str) -> int:
    ingredients: List[Tuple[int, int, int, int]] = [parse_line(line)[:-1] for line in input.splitlines()]

    best_total = max(get_total(ingredients, permutation) for permutation in get_permutations(len(ingredients)))

    return best_total


def solution2(input: str) -> int:
    parsed: List[Tuple[int, ...]] = [parse_line(line) for line in input.splitlines()]
    ingredients: List[Tuple[int, ...]] = [item[:-1] for item in parsed]
    calories: List[int] = [item[-1] for item in parsed]

    return max(get_total(ingredients, perm) for perm in get_permutations(len(ingredients)) if get_calories(calories, perm) == 500)
