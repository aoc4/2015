import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input = """Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"""
        expected = 62842880
        self.assertEqual(expected, day.solution1(input))

    def test(self):
        input = """Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3"""
        expected = 57600000
        self.assertEqual(expected, day.solution2(input))