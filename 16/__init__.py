from pathlib import Path

from . import day

ticker_tape = """children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1"""

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read()
    print(f"solution1: {day.solution1(ticker_tape, data)}")
    print(f"solution2: {day.solution2(ticker_tape, data)}")