from typing import Dict

import re

class Aunt:
    def __init__(self, attrs: Dict[str, int]):
        self.attrs = attrs

    def __eq__(self, other: "Aunt"):
        """compare aunts.
        Only attributes that are set in self are compared
        """
        return all(other.attrs[key] == value for key, value in self.attrs.items())


def parse_aunt(line: str) -> Dict[str, int]:
    matches = re.findall(r"(\S+): (\d+)", line)
    return dict(matches)  # pretty cool that dict accept [(k, v)]


def solution1(ticket: str, data: str):
    suspect_aunt = Aunt(parse_aunt(ticket))
    aunts = [Aunt(parse_aunt(line)) for line in data.splitlines()]
    return next(index + 1 for index, aunt in enumerate(aunts) if aunt == suspect_aunt)


class Aunt2:
    def __init__(self, attrs: Dict[str, int]):
        self.attrs = attrs

    def __eq__(self, other: "Aunt"):
        """compare aunts.
        Only attributes that are set in self are compared
        """
        more_than = ["cats", "trees"]
        less_than = ["pomeranians", "goldfish"]
        leftover = {k: v for k, v in self.attrs.items() if k not in more_than and k not in less_than}
        cond_more_than = all(self.attrs[attr] > other.attrs[attr] for attr in more_than if attr in self.attrs)
        cond_less_than = all(self.attrs[attr] < other.attrs[attr] for attr in less_than if attr in self.attrs)
        cond_leftover = all(other.attrs[key] == value for key, value in leftover.items())
        return cond_more_than and cond_less_than and cond_leftover

def solution2(ticket: str, data: str):
    suspect_aunt = Aunt2(parse_aunt(ticket))
    aunts = [Aunt2(parse_aunt(line)) for line in data.splitlines()]
    return next(index + 1 for index, aunt in enumerate(aunts) if aunt == suspect_aunt)