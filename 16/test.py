from pathlib import Path
import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input1 = """cars: 9
akitas: 3
goldfish: 0
"""
        input2 = """Sue 1: cars: 9, akitas: 3, goldfish: 0"""
        
        expected = 1
        self.assertEqual(expected, day.solution1(input1, input2))

    def test_data(self):
        ticker_tape = """children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1"""
        input_path = Path(__file__).parent
        with open(input_path / "input.txt") as f:
            data = f.read()
        print(f"solution1: {day.solution1(ticker_tape, data)}")
        print(f"solution2: {day.solution2(ticker_tape, data)}")