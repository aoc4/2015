from pathlib import Path

from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = [int(line) for line in f.read().splitlines()]

    eggnog = 150
    print(f"solution1: {len(day.solution1(eggnog, data))}")
    print(f"solution1: {len(day.solution2(eggnog, data))}")