from typing import List, Tuple

from itertools import combinations

def get_all_combinations(containers: List[int]):
    combos = []

    for idx in range(1, len(containers) + 1):
        combos.extend(combinations(containers, idx))

    return combos


def solution1(eggnog: int, containers: List[int]) -> List[Tuple[int, ...]]:
    combinations = get_all_combinations(containers)
    filtered = [combo for combo in combinations if sum(combo) == eggnog]

    return sorted(filtered)


def solution2(eggnog: int, containers: List[int]) -> List[Tuple[int, ...]]:
    combined = solution1(eggnog, containers)
    smallest = min(len(combo) for combo in combined)
    filtered = [combo for combo in combined if len(combo) == smallest]
    return filtered
