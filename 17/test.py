import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input1 = 25
        input2 = [20, 15, 10, 5, 5]
        expected = sorted([
            (15, 10),
            (20, 5),
            (20, 5),
            (15, 5, 5)
        ])
        self.assertListEqual(expected, day.solution1(input1, input2))

    def test2(self):
        input1 = 25
        input2 = [20, 15, 10, 5, 5]
        expected = sorted([
            (15, 10),
            (20, 5),
            (20, 5),
        ])
        self.assertListEqual(expected, day.solution2(input1, input2))
