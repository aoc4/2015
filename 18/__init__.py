from pathlib import Path

from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read()

    steps = 100
    print(f"solution1: {day.solution1(data, steps)}")
    print(f"solution2: {day.solution2(data, steps)}")