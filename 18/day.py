from typing import List


def parse_data(data: str) -> List[List[int]]:
    return [[int("#" == char) for char in line] for line in data.splitlines()]


def get_on_neighbours(x: int, y: int, lights: List[List[int]]) -> int:
    x_min = max(0, x - 1)
    x_max = min(len(lights) - 1, x + 1) + 1
    y_min = max(0, y - 1)
    y_max = min(len(lights[0]), y + 1) + 1

    return sum(sum(lights[x][y_min: y_max]) for x in range(x_min, x_max)) - lights[x][y]


def iterate(lights: List[List[int]]) -> List[List[int]]:
    new_lights: List[List[int]] = [row[:] for row in lights]

    for row_idx in range(len(lights)):
        for col_idx in range(len(lights[row_idx])):
            neighbours_on = get_on_neighbours(row_idx, col_idx, lights)
            if lights[row_idx][col_idx] == 1:
                new_lights[row_idx][col_idx] = int(neighbours_on in [2, 3])
            else:
                new_lights[row_idx][col_idx] = int(neighbours_on == 3)

    return new_lights


def solution1(data: str, steps: int) -> int:
    lights = parse_data(data)
    for _ in range(steps):
        lights = iterate(lights)

    return sum(sum(line) for line in lights)


def light_corners(lights: List[List[int]]) -> List[List[int]]:
    lights[0][0] = 1
    lights[0][-1] = 1
    lights[-1][0] = 1
    lights[-1][-1] = 1
    return lights


def solution2(data: str, steps: int) -> int:
    lights = light_corners(parse_data(data))
    for _ in range(steps):
        lights = light_corners(iterate(lights))

    return sum(sum(line) for line in lights)

"""4
##.###
.##..#
.##...
.##...
#.#...
##...#
"""