import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        steps = 1
        state = """.#.#.#
...##.
#....#
..#...
#.#..#
####.."""
        expected = 11
        self.assertEqual(expected, day.solution1(state, steps))

    def test2(self):
        steps = 2
        state = """.#.#.#
...##.
#....#
..#...
#.#..#
####.."""
        expected = 8
        self.assertEqual(expected, day.solution1(state, steps))

    def test3(self):
        steps = 3
        state = """.#.#.#
...##.
#....#
..#...
#.#..#
####.."""
        expected = 4
        self.assertEqual(expected, day.solution1(state, steps))
        
    def test4(self):
        steps = 4
        state = """.#.#.#
...##.
#....#
..#...
#.#..#
####.."""
        expected = 4
        self.assertEqual(expected, day.solution1(state, steps))

    def test5(self):
        steps = 1
        state = """##.#.#
...##.
#....#
..#...
#.#..#
####.#"""
        expected = 18
        self.assertEqual(expected, day.solution2(state, steps))

    def test6(self):
        steps = 2
        state = """##.#.#
...##.
#....#
..#...
#.#..#
####.#"""
        expected = 18
        self.assertEqual(expected, day.solution2(state, steps))

    def test7(self):
        steps = 3
        state = """##.#.#
...##.
#....#
..#...
#.#..#
####.#"""
        expected = 18
        self.assertEqual(expected, day.solution2(state, steps))

    def test8(self):
        steps = 4
        state = """##.#.#
...##.
#....#
..#...
#.#..#
####.#"""
        expected = 14
        self.assertEqual(expected, day.solution2(state, steps))

    def test9(self):
        steps = 5
        state = """##.#.#
...##.
#....#
..#...
#.#..#
####.#"""
        expected = 17
        self.assertEqual(expected, day.solution2(state, steps))
