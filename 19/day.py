from typing import Dict, List, Set, Tuple

import re


def parse_data(data: str) -> Tuple[Dict[str, List[str]], str]:
    lines = data.strip().splitlines()
    molecule = lines[-1].strip()

    parsed = [line.strip().split(" => ") for line in lines[:-2]]
    replacements = {}
    for key, value in parsed:
        if key in replacements:
            replacements[key].append(value)
        else:
            replacements[key] = [value]
    return replacements, molecule


def get_molecule(molecule: str, key: str, value: str) -> List[str]:
    starts = [m.start() for m in re.finditer(key, molecule)]
    key_size = len(key)
    return [molecule[: i] + value + molecule[i + key_size:] for i in starts]


def get_molecules(molecule: str, key: str, values: List[str]) -> List[str]:
    molecules = []
    for value in values:
        molecules.extend(get_molecule(molecule, key, value))

    return molecules


def get_variations(molecule: str, replacements: Dict[str, List[str]]) -> Set[str]:
    variations = set()
    for key, values in replacements.items():
        for new_molecule in get_molecules(molecule, key, values):
            variations.add(new_molecule)

    return variations


def solution1(data: str) -> int:
    replacements, molecule = parse_data(data)
    variations = get_variations(molecule, replacements)

    return len(variations)


def solution2(data: str) -> int:
    replacements, molecule = parse_data(data)

    step = set(["e"])
    steps = 0
    while molecule not in step:
        new_step = set()
        steps += 1
        for s in step:
            for variation in get_variations(s, replacements):
                new_step.add(variation)
        step = new_step.copy()
        print(steps, len(step))

    return steps