import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """H => HO
H => OH
O => HH

HOH"""
        expected = 4
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """H => HO
H => OH
O => HH

HOHOHO"""
        expected = 7
        self.assertEqual(expected, day.solution1(data))

    def test3(self):
        data = """e => H
e => O
H => HO
H => OH
O => HH

HOH"""
        expected = 3
        self.assertEqual(expected, day.solution2(data))

    def test4(self):
        data = """e => H
e => O
H => HO
H => OH
O => HH

HOHOHO"""
        expected = 6
        self.assertEqual(expected, day.solution2(data))