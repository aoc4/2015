from functools import reduce
import operator


def solution1(input: str) -> int:
    l, w, h = (int(side) for side in input.split("x"))

    surface = 2 * (l * w + w * h + h * l)

    # smallest side dimensions multiplied
    slack = reduce(operator.mul, sorted([l, w, h])[:2])

    return surface + slack
    


def solution2(input: str) -> int:
    l, w, h = (int(side) for side in input.split("x"))

    # two smallest side dimensions 
    ribbon = 2 * sum(sorted([l, w, h])[:2])

    bow = l * w * h

    return ribbon + bow