import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = "2x3x4"
        expected = 58
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_2(self):
        test_string = "1x1x10"
        expected = 43
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution2_1(self):
        test_string = "2x3x4"
        expected = 34
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_2(self):
        test_string = "1x1x10"
        expected = 14
        self.assertEqual(day.solution2(test_string), expected)