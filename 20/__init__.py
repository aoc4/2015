from pathlib import Path

from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    score = 34000000
    delivery_stop = 50

    print(f"solution1: {day.solution1(score, multiplier=10)}")
    print(f"solution2: {day.solution2(score, delivery_stop, multiplier=11)}")