from functools import reduce
from typing import Set
from math import sqrt

# from https://stackoverflow.com/a/19578818/552214
def factors(n: int) -> Set[int]:
    step = 2 if n % 2 else 1
    return set(reduce(list.__add__,
               ([i, n // i] for i in range(1, int(sqrt(n)) + 1, step) if n % i == 0)))


def get_house_score(number: int, multiplier: int) -> int:
    return multiplier * sum(factors(number))


def solution1(min_score: int, multiplier: int = 10) -> int:
    house_number = 1
    while get_house_score(house_number, multiplier) < min_score:
        house_number += 1

    return house_number


def get_limited_house_score(number: int, stop: int, multiplier: int) -> int:
    # i.e. 2 appears 5th time when in number == 10
    factor_sum = sum(fac for fac in factors(number) if number // fac <= stop)
    return multiplier * factor_sum


def solution2(min_score: int, delivery_stop: int, multiplier: int) -> int:
    house_number = 1
    while get_limited_house_score(house_number, delivery_stop, multiplier) < min_score:
        house_number += 1

    return house_number
