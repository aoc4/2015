import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        score = 5
        expected = 1
        self.assertEqual(expected, day.solution1(score))

    def test2(self):
        score = 30
        expected = 2
        self.assertEqual(expected, day.solution1(score))

    def test3(self):
        score = 39
        expected = 3
        self.assertEqual(expected, day.solution1(score))

    def test4(self):
        score = 100
        expected = 6
        self.assertEqual(expected, day.solution1(score))

    def test5(self):
        score = 140
        expected = 8
        self.assertEqual(expected, day.solution1(score))
