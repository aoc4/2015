import re
import math
from dataclasses import dataclass
from typing import Iterable, List, Tuple


def get_death_turn(player: "Player", dmg: int) -> int:
    damage_per_turn = max(1, dmg - player.armor)
    return math.ceil(player.hitpoints / damage_per_turn)


@dataclass
class Item:
    price: int
    damage: int
    armor: int

    def __eq__(self, other: "Item"):
        return self.price == other.price and self.damage == other.damage and self.armor == other.armor


@dataclass
class Shop:
    weapons: List[Item]
    armors: List[Item]
    rings: List[Item]


@dataclass
class Player:
    hitpoints: int = 0
    armor: int = 0
    damage: int = 0

    def can_win_against(self, other: "Player") -> bool:
        # assuming (self) starts, turn based same amount of turns equal me hitting death blow first
        return get_death_turn(self, other.damage) >= get_death_turn(other, self.damage)

    def apply_items(self, items: Tuple[Item, ...]):
        for item in items:
            self.armor += item.armor
            self.damage += item.damage


def parse_boss_data(data: str) -> Player:
    hp_regex = re.compile(r"Hit Points: (\d+)", re.MULTILINE)
    dmg_regex = re.compile(r"Damage: (\d+)", re.MULTILINE)
    armor_regex = re.compile(r"Armor: (\d+)", re.MULTILINE)
    hp = int(hp_regex.search(data).group(1))
    dmg = int(dmg_regex.search(data).group(1))
    armor = int(armor_regex.search(data).group(1))
    return Player(hitpoints=hp, armor=armor, damage=dmg)


def parse_shop_item(line: str) -> Item:
    match = re.match(r"\S+(?:\s\+\d)?\s+(\d+)\s+(\d+)\s+(\d+)", line)
    price = int(match.group(1))
    damage = int(match.group(2))
    armor = int(match.group(3))
    return Item(price=price, damage=damage, armor=armor)


def parse_shop_data(shop_data: str):
    categories = shop_data.split("\n\n")
    weapons = []
    armors = []
    rings = []
    for category in categories:
        items = [parse_shop_item(line) for line in category.splitlines()[1:]]
        if category.startswith("Weapons:"):
            weapons = items
        elif category.startswith("Armor:"):
            armors = items
        elif category.startswith("Rings:"):
            rings = items

    return Shop(weapons=weapons, armors=armors, rings=rings)


def get_item_combinations(shop: Shop) -> Iterable[Tuple[Item, ...]]:
    for weapon in shop.weapons:
        for armor in shop.armors:
            for ring1 in shop.rings:
                leftover_rings = shop.rings.copy()
                if ring1 != Item(0, 0, 0):
                    leftover_rings.remove(ring1)
                for ring2 in leftover_rings:
                    yield weapon, armor, ring1, ring2


def solution1(boss_data: str, shop_data: str, hitpoints: int):
    boss = parse_boss_data(boss_data)
    shop = parse_shop_data(shop_data)

    # constraints:
    # weapons: 1
    # armor: 0-1
    # rings: 0-2

    # since armor and rings are optional, just add empty items
    shop.armors.append(Item(0, 0, 0))
    shop.rings.append(Item(0, 0, 0))

    prices = []
    for combination in get_item_combinations(shop):
        player = Player(hitpoints=hitpoints)
        player.apply_items(combination)
        if player.can_win_against(boss):
            prices.append(sum(item.price for item in combination))

    return min(prices)


def solution2(boss_data: str, shop_data: str, hitpoints: int):
    boss = parse_boss_data(boss_data)
    shop = parse_shop_data(shop_data)
    shop.armors.append(Item(0, 0, 0))
    shop.rings.append(Item(0, 0, 0))

    prices = []
    for combination in get_item_combinations(shop):
        player = Player(hitpoints=hitpoints)
        player.apply_items(combination)
        if not player.can_win_against(boss):
            prices.append(sum(item.price for item in combination))

    return max(prices)
