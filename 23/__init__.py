from pathlib import Path

from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        instructions = f.read()

    ans1 = day.solution1(instructions)
    ans2 = day.solution2(instructions)

    print(f"solution1: {ans1['b']}")
    print(f"solution2: {ans2['b']}")
