from typing import Dict, List


def run_instructions(registers: Dict[str, int], instructions: List[str]) -> Dict[str, int]:
    index: int = 0

    while index < len(instructions):
        instr, *params = instructions[index].split()

        if instr == "hlf":
            register, *_ = params
            registers[register] //= 2
            index += 1
        elif instr == "tpl":
            register, *_ = params
            registers[register] *= 3
            index += 1
        elif instr == "inc":
            register, *_ = params
            registers[register] += 1
            index += 1
        elif instr == "jmp":
            jump, *_ = params
            index += int(jump)
        elif instr == "jie":
            register, jump = params
            register = register[:-1]  # remove comma 
            if registers[register] % 2 == 0:
                index += int(jump)
            else:
                index += 1
        elif instr == "jio":
            register, jump = params
            register = register[:-1]  # remove comma 
            if registers[register] == 1:
                index += int(jump)
            else:
                index += 1
    return registers

def solution1(data: str) -> Dict[str, int]:
    start_registers = {"a": 0, "b": 0}
    instructions = data.splitlines()
    return run_instructions(start_registers, instructions)

def solution2(data: str) -> Dict[str, int]:
    start_registers = {"a": 1, "b": 0}
    instructions = data.splitlines()
    return run_instructions(start_registers, instructions)