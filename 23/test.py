import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        instructions = """inc a
jio a, +2
tpl a
inc a"""
        expected = {"a": 2, "b": 0}
        self.assertEqual(expected, day.solution1(instructions))
