from typing import Iterable, List, Tuple

import math
from itertools import combinations


def get_combination_group(packages: List[int], target_weight: int) -> Iterable[Tuple[int, ...]]:
    for length in range(2, len(packages)):
        combos = [c for c in combinations(packages, length) if sum(c) == target_weight]
        if combos:
            for combo in combos:
                yield combo
            break  # don't need longer viable packages


def get_answer(packages: List[int], weight_per_package: int) -> int:
    groups = [group for group in get_combination_group(packages, weight_per_package)]
    shortest_group = min(len(group) for group in groups)

    # candidates for quantum stuff
    shortest_candidates = [group for group in groups if len(group) == shortest_group]

    return min(math.prod(group) for group in shortest_candidates)


def solution1(data: str) -> int:
    packages = [int(package) for package in data.splitlines()]
    weight_per_package = sum(packages) // 3
    return get_answer(packages, weight_per_package)
    

def solution2(data: str) -> int:
    packages = [int(package) for package in data.splitlines()]
    weight_per_package = sum(packages) // 4
    return get_answer(packages, weight_per_package)