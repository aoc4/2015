from pathlib import Path
import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        packages = """1
2
3
4
5
7
8
9
10
11"""
        expected = 99
        self.assertEqual(expected, day.solution1(packages))

    def test2(self):
        input_path = Path(__file__).parent
        with open(input_path / "input.txt") as f:
            packages = f.read()
        day.solution1(packages)
