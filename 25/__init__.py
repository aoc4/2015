from . import day

if __name__ == "__main__":
    row = 2981
    column = 3075
    start = 20151125
    print(f"solution1: {day.solution1(row, column, start)}")
