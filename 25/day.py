def solution1(target_row: int, target_column: int, last: int) -> int:
    row = 1
    column = 1
    multiplier = 252533
    divider = 33554393
    while row != target_row or column != target_column:
        if row == 1:
            row = column + 1
            column = 1
        else:
            row -= 1
            column += 1
        last = (last * multiplier) % divider
    return last
