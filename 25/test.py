import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        start = 20151125
        row = 1
        column = 2
        expected = 18749137
        self.assertEqual(expected, day.solution1(row, column, start))

    def test2(self):
        start = 20151125
        row = 4
        column = 4
        expected = 9380097
        self.assertEqual(expected, day.solution1(row, column, start))

    def test3(self):
        start = 20151125
        row = 6
        column = 6
        expected = 27995004
        self.assertEqual(expected, day.solution1(row, column, start))