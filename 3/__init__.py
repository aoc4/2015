from pathlib import Path
from inspect import getmembers, isfunction
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    functions = getmembers(day, isfunction)
    for name, func in functions:
        if name != "get_houses_visited":
            print(f"{name}: {func(contents)}")