from typing import Set, Tuple


def get_houses_visited(input: str) -> Set[Tuple[int, int]]:
    houses: Set[Tuple[int, int]] = {(0, 0)}  # starting position
    x, y = 0, 0  # current coordinates

    for dir in input:
        if dir == ">":
            x += 1
        elif dir == "<":
            x -= 1
        elif dir == "^":
            y += 1
        elif dir == "v":
            y -= 1

        houses.add((x, y))
    
    return houses




def solution1(input: str) -> int:
    return len(get_houses_visited(input))


def solution2(input: str) -> int:
    # same as solution1, just need to split input
    real_santa = input[0::2]
    robo_santa = input[1::2]

    real_houses = get_houses_visited(real_santa)
    robo_houses = get_houses_visited(robo_santa)

    return len(real_houses | robo_houses)
