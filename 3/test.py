import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = ">"
        expected = 2
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_2(self):
        test_string = "^>v<"
        expected = 4
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_3(self):
        test_string = "^v^v^v^v^v"
        expected = 2
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution2_1(self):
        test_string = "^v"
        expected = 3
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_2(self):
        test_string = "^>v<"
        expected = 3
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_3(self):
        test_string = "^v^v^v^v^v"
        expected = 11
        self.assertEqual(day.solution2(test_string), expected)