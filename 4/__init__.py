from . import day

# not to exhaust CI, use different name
if __name__ == "__really_main__":
    input = "yzbqklnj"

    print(f"solution1: {day.solution1(input=input, lead_zeros=5)}")
    print(f"solution2: {day.solution1(input=input, lead_zeros=6)}")