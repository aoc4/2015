import hashlib


def solution1(input: str, lead_zeros: int) -> int:
    number = 1
    hash = hashlib.md5(f"{input}{number}".encode()).hexdigest()
    while not hash.startswith("0" * lead_zeros):
        number += 1
        hash = hashlib.md5(f"{input}{number}".encode()).hexdigest()

    return number