import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = "abcdef"
        expected = 609043
        self.assertEqual(day.solution1(test_string, lead_zeros=5), expected)

    def test_solution1_2(self):
        test_string = "pqrstuv"
        expected = 1048970
        self.assertEqual(day.solution1(test_string, lead_zeros=5), expected)
