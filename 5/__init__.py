from pathlib import Path
from inspect import getmembers, isfunction
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    functions = getmembers(day, isfunction)
    for name, func in functions:
        total = 0

        for row in contents.splitlines():
            total += int(func(row))
        
        print(f"{name}: {total}")