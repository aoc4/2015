from collections import Counter
import re


def solution1(input: str) -> bool:
    # It contains at least three vowels (aeiou only)
    count = Counter(input)
    vowels = "aeiou"
    has_3_vowels = sum(count[vowel] for vowel in vowels) >= 3

    # It contains at least one letter that appears twice in a row
    has_double = any(first == second for first, second in zip(input, input[1:]))

    # It does not contain the strings ab, cd, pq, or xy
    blacklist = ["ab", "cd", "pq", "xy"]
    no_specific_pairs = not any(item in input for item in blacklist)    

    return has_3_vowels and has_double and no_specific_pairs


def solution2(input: str) -> bool:
    # It contains a pair of any two letters that appears at least twice in the string without overlapping
    pairs = [''.join(pair) for pair in zip(input, input[1:])]
    pair_count = [input.count(pair) for pair in pairs]
    has_2ormore_pairs = max(pair_count) >= 2

    # It contains at least one letter which repeats with exactly one letter between them
    has_letter_between_same = any(input[i] == input[i + 2] for i in range(len(input[:-2])))

    return has_2ormore_pairs and has_letter_between_same
