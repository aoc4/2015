import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = "ugknbfddgicrmopn"
        expected = True
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_2(self):
        test_string = "aaa"
        expected = True
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_3(self):
        test_string = "jchzalrnumimnmhp"
        expected = False
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_4(self):
        test_string = "haegwjzuvuyypxyu"
        expected = False
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_5(self):
        test_string = "dvszwmarrgswjxmb"
        expected = False
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution2_1(self):
        test_string = "qjhvhtzxzqqjkmpb"
        expected = True
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_2(self):
        test_string = "xxyxx"
        expected = True
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_2_1(self):
        test_string = "aaa"
        expected = False
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_3(self):
        test_string = "uurcxstgmygtbstg"
        expected = False
        self.assertEqual(day.solution2(test_string), expected)

    def test_solution2_4(self):
        test_string = "ieodomkazucvgmuy"
        expected = False
        self.assertEqual(day.solution2(test_string), expected)