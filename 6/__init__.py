from pathlib import Path
from inspect import getmembers, isfunction
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    # solution1
    ans1 = 0
    for row in contents.splitlines():
        ans1 = day.solution1(row)
    print(f"solution1: {ans1}")

    # solution2
    ans2 = 0
    for row in contents.splitlines():
        ans2 = day.solution2(row)
    print(f"solution2: {ans2}")