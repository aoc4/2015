from typing import List, Tuple, Callable, Dict

import re


def parse_instruction(line: str) -> Tuple[str, Tuple[int, int], Tuple[int, int]]:
    matcher = re.match(r"(\w*\s*?\w*?)\s*(\d+),(\d+) through (\d+),(\d+)", line)
    if matcher:
        action = matcher.group(1)
        from_x = int(matcher.group(2))
        from_y = int(matcher.group(3))
        to_x = int(matcher.group(4))
        to_y = int(matcher.group(5))
        return action, (from_x, from_y), (to_x, to_y)

    raise Exception(f"CAN'T PARSE INSTRUCTIONS {line}")


def solve_grid(input: str, grid: List[List[int]], ACTIONS: Dict[str, Callable[[int], int]]):
    for line in input.splitlines():
        action, start, end = parse_instruction(line)
        for row in range(start[0], end[0] + 1):
            for col in range(start[1], end[1] + 1):
                grid[row][col] = ACTIONS[action](grid[row][col])
    return sum(sum(line) for line in grid)


def solution1(input: str, grid: List[List[int]] = [[0] * 1000 for _ in range(1000)]) -> int:
    ACTIONS: Dict[str, Callable[[int], int]] = {
        "turn on": lambda x: 1,
        "turn off": lambda x: 0,
        "toggle": lambda x: int(not x)
    }

    return solve_grid(input, grid, ACTIONS)


def solution2(input: str, grid: List[List[int]] = [[0] * 1000 for _ in range(1000)]) -> int:
    ACTIONS: Dict[str, Callable[[int], int]] = {
        "turn on": lambda x: x + 1,
        "turn off": lambda x: max(x - 1, 0),
        "toggle": lambda x: x + 2
    }

    return solve_grid(input, grid, ACTIONS)
