import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_solution1_1(self):
        test_string = "turn on 0,0 through 999,999"
        expected = 1 * 1000 * 1000
        self.assertEqual(day.solution1(test_string, [[0] * 1000] * 1000), expected)

    def test_solution1_2(self):
        test_string = "toggle 0,0 through 999,0"
        expected = 1 * 1000
        self.assertEqual(day.solution1(test_string, [[0] * 1000 for _ in range(1000)]), expected)

    def test_solution2_1(self):
        test_string = "turn on 0,0 through 0,0"
        expected = 1
        self.assertEqual(day.solution2(test_string, [[0] * 1000 for _ in range(1000)]), expected)

    def test_solution2_2(self):
        test_string = "toggle 0,0 through 999,999"
        expected = 2000000
        self.assertEqual(day.solution2(test_string, [[0] * 1000 for _ in range(1000)]), expected)