from pathlib import Path
from inspect import getmembers, isfunction
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    lines = contents.splitlines()
    # solution1
    ans1 = day.solution1(lines)
    print(f"solution1: {ans1['a']}")

    # solution2
    lines[3] = f"{ans1['a']} -> b"
    day.registry.clear()
    ans2 = day.solution1(lines)
    print(f"solution2: {ans2['a']}")