from typing import List, Tuple, Union, Dict
import re

registry: Dict[str, int] = {}


def parse_statement(line: str) -> Tuple[str, Union[str, None], Union[str, None], str]:
    matches = re.match(r"(?:(?:(\S+) )?(.*) )?(\S+) -> (\S+)", line)
    return matches[1], matches[2], matches[3], matches[4]


def resolve(item: Union[str, None]) -> Union[int, None]:
    """Try to figure out keyword's value.
    If the item does not exist in registry -> None
    If the item is None -> None
    If the item is a digit -> int
    """
    if item is not None:
        if item.isdigit():
            return int(item)
        elif item in registry:
            return registry[item]


def solution1(input: List[str], reg: Dict[str, int] = registry) -> Dict[str, int]:
    registry = reg

    unresolved = 1
    while unresolved != 0:  # force sequential resolve
        unresolved = 0
        for line in input:
            left, op, right, targ = parse_statement(line)

            left = resolve(left)
            right = resolve(right)

            if op == "AND":
                if left is not None and right is not None:
                    registry[targ] = left & right
                else:
                    unresolved += 1
            elif op == "OR":
                if left is not None and right is not None:
                    registry[targ] = left | right
                else:
                    unresolved += 1
            elif op == "NOT":
                if right is not None:
                    registry[targ] = ~right
                else:
                    unresolved += 1
            elif op == "LSHIFT":
                if left is not None and right is not None:
                    registry[targ] = left << right
                else:
                    unresolved += 1
            elif op == "RSHIFT":
                if left is not None and right is not None:
                    registry[targ] = left >> right
                else:
                    unresolved += 1
            elif op is None:  # assignment
                if right is not None:
                    # print(targ, right)
                    registry[targ] = right
                else:
                    unresolved += 1

    # remap everything to 16bit

    return {k: v & 0xFFFF for k, v in registry.items()}