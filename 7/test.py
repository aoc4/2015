import unittest

from . import day


class DayTest(unittest.TestCase):

    def setUp(self) -> None:
        day.registry.clear()

    def test_solution1_assign(self):
        test_string = ["123 -> x"]
        expected = {"x": 123}
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_and(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "x AND y -> d"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "d": 72
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_or(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "x OR y -> e"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "e": 507
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_lshift(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "x LSHIFT 2 -> f"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "f": 492
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_rshift(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "y RSHIFT 2 -> g"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "g": 114
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_not1(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "NOT x -> h"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "h": 65412
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_not2(self):
        test_string = [
            "123 -> x",
            "456 -> y",
            "NOT y -> i"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "i": 65079
        }
        self.assertEqual(day.solution1(test_string), expected)

    def test_solution1_late(self):
        test_string = [
            "NOT y -> i",
            "123 -> x",
            "456 -> y"
        ]
        expected = {
            "x": 123,
            "y": 456,
            "i": 65079
        }
        self.assertEqual(day.solution1(test_string), expected)
