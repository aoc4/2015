from pathlib import Path
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    in_memory = sum(day.solution1(line) for line in contents.splitlines())
    in_string = sum(len(line) for line in contents.splitlines())
    print(f"solution1: {in_string - in_memory}")

    ans2 = sum(day.solution2(line) for line in contents.splitlines())

    print(f"solution2: {ans2}")