import re


def solution1(input: str) -> int:
    # remove surrounding quotes
    unquoted = input[1:-1]

    # hex escapes
    hex_escape_size = 4
    hex_escapes = len(re.findall(r"\\x[0-9a-f]{2}", unquoted))
    
    # single symbol escape
    single_escape_size = 2
    single_escapes = len(re.findall(r"\\\S", unquoted)) - hex_escapes

    without_escape = len(unquoted) - hex_escapes * hex_escape_size - single_escapes * single_escape_size
    with_escapes = without_escape + hex_escapes + single_escapes
    return with_escapes

def solution2(input: str) -> int:
    quotes = input.count('"')
    escapes = input.count('\\')

    return quotes + escapes + 2