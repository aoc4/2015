import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1_empty(self):
        input = '""'
        expected = 0
        self.assertEqual(expected, day.solution1(input))

    def test1_abc(self):
        input = '"abc"'
        expected = 3
        self.assertEqual(expected, day.solution1(input))

    def test1_esc_quote(self):
        input = r'"aaa\"aaa"'
        expected = 7
        self.assertEqual(expected, day.solution1(input))

    def test1_hex(self):
        input = r'"\x27"'
        expected = 1
        self.assertEqual(expected, day.solution1(input))

    def test2_empty(self):
        input = '""'
        expected = 6 - 2
        self.assertEqual(expected, day.solution2(input))

    def test2_abc(self):
        input = '"abc"'
        expected = 9 - 5
        self.assertEqual(expected, day.solution2(input))

    def test2_esc_quote(self):
        input = r'"aaa\"aaa"'
        expected = 16 - 10
        self.assertEqual(expected, day.solution2(input))

    def test2_hex(self):
        input = r'"\x27"'
        expected = 11 - 6
        self.assertEqual(expected, day.solution2(input))
