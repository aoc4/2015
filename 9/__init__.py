from pathlib import Path
from . import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    contents = Path(input_path / "input.txt").read_text()

    print(f"solution1: {day.solution1(contents)}")
    print(f"solution2: {day.solution2(contents)}")