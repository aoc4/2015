from typing import Dict, List, Tuple

import re

from itertools import permutations

def get_line_data(line: str) -> Tuple[str, str, int]:
    matches = re.match(r"(\S+) to (\S+) = (\d+)", line)
    return matches.group(1), matches.group(2), int(matches.group(3))

def get_cities_and_distances(input):
    cities = set()
    distances: Dict[str, Dict[str, int]] = {}
    for line in input.splitlines():
        src, trg, dist = get_line_data(line)
        cities.add(src)
        cities.add(trg)

        if src not in distances:
            distances[src] = {}

        if trg not in distances:
            distances[trg] = {}

        distances[src][trg] = dist
        distances[trg][src] = dist
    return cities, distances


def solution1(input: str) -> int:
    cities, distances = get_cities_and_distances(input)

    # go through all permutations
    shortest_distance = None
    for permutation in permutations(cities):
        distance = sum(distances[a][b] for a, b in zip(permutation, permutation[1:]))
        shortest_distance = min(shortest_distance, distance) if shortest_distance else distance

    return shortest_distance


def solution2(input: str) -> int:
    cities, distances = get_cities_and_distances(input)

    longest_distance = None
    for permutation in permutations(cities):
        distance = sum(distances[a][b] for a, b in zip(permutation, permutation[1:]))
        longest_distance = max(longest_distance, distance) if longest_distance else distance

    return longest_distance