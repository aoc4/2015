import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        input = """London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
"""
        expected = 605
        self.assertEqual(expected, day.solution1(input))

    def test2(self):
        input = """London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
"""
        expected = 982
        self.assertEqual(expected, day.solution2(input))
